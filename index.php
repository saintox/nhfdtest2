<!doctype html>
<html lang='en'>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Niagahoster</title>
        
        <!-- FA -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

        <!-- CUSTOM CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        
        <!-- JQUERY -->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    </head>

    <body>
        <section class="shadow-sm">
            <div class="container">
                <table width="100%">
                    <tr>
                        <td width="60%">
                            <span class="small">Gratis Ebook 9 Cara Cerdas Menggunakan Domain <span class="btn-close">[x]</span></span>
                        </td>
                        <td width="40%" style="font-size:.8em;" align="right">
                            <span><i class="fa fa-phone"></i> 0274-5305505</span>
                            <span><i class="fa fa-comments"></i> Live Chat</span>
                            <span><i class="fa fa-users"></i> Member Area</span>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
        <section class="shadow-sm">
            <div class="container">
                <table width="100%" border="0">
                    <tr>
                        <td width="20%">
                            <a href="" class="navbar-brand"><img src="assets/images/niagahoster.jpg" alt="logo"></a>
                        </td>
                        <td width="80%">
                            <nav class="nav justify-content-end">
                                <a href="" class="nav-link px-2">Hosting</a>
                                <a href="" class="nav-link px-2">Domain</a>
                                <a href="" class="nav-link px-2">Server</a>
                                <a href="" class="nav-link px-2">Website</a>
                                <a href="" class="nav-link px-2">Afiliasi</a>
                                <a href="" class="nav-link px-2">Promo</a>
                                <a href="" class="nav-link px-2">Pembayaran</a>
                                <a href="" class="nav-link px-2">Review</a>
                                <a href="" class="nav-link px-2">Kontak</a>
                                <a href="" class="nav-link px-2">Blog</a>
                            </nav>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
        <section class="shadow-sm">
        <div class="container">
            <table width="100%" border="0">
                <tr>
                    <td width="50%">
                        <h1 class="hosting-title">PHP Hosting</h1>
                        <p class="hosting-caption">Cepat, handal, penuh dengan <br> modul PHP yang anda butuhkan</p>
                        <ul class="hosting-list" style="float:left">
                            <li><span class="fa fa-check-circle text-success"></span> Solusi PHP untuk performa query yang lebih cepat.</li>
                            <li><span class="fa fa-check-circle text-success"></span> Konsumsi memori yang lebih rendah.</li>
                            <li><span class="fa fa-check-circle text-success"></span> Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 7</li>
                            <li><span class="fa fa-check-circle text-success"></span> Fitur enkripsi ionCube dan ZenGuard Loaders</li>
                        </ul>
                    </td>
                    <td width="50%">
                        <img class="hosting-banner banner-img float-right" src="assets/svg/illustration banner PHP hosting-01.svg">
                    </td>
                </tr>
            </table>
        </div>
        </section>
        <section>
        <div class="container">
            <table width="100%" border="0">
                <tr>
                    <td width="33.3%">
                        <center>
                            <object class="feature-image" data="assets/svg/icon PHP Hosting_zendguard.svg" alt="PHP ZenGuard Loader" type="image/svg+xml"></object>
                            <p class="feature-caption">PHP Zen Guard Loader</p>
                        </center>    
                    </td>
                    <td width="33.3%">
                        <center>
                            <object class="feature-image" data="assets/svg/icon PHP Hosting_composer.svg" alt="PHP Composer" type="image/svg+xml"></object>
                            <p class="feature-caption">PHP Composer</p>
                        </center>    
                    </td>
                    <td width="33.3%">
                        <center>
                        <object class="feature-image" data="assets/svg/icon PHP Hosting_ioncube.svg" alt="PHP Ion Cube" type="image/svg+xml"></object>
                            <p class="feature-caption">PHP Ion Cube Loader</p>
                        </center>    
                    </td>
                </tr>
            </table>
        </div>
        </section>
        <section>
        <div class="container">
            <table width="100%" border="0">
                <tr>
                    <td>
                       <center>
                            <h2 class="paket-title">Paket Hosting Singapura yang Tepat</h2>
                            <p class="paket-caption">Diskon 40% + Domain dan SSL Gratis untuk Anda</p>
                       </center> 
                    </td>
                </tr>
            </table>
        </div>
        </section>
        <section>
        <div class="container">
            <table width="100%" class="tabel-paket table-bordered">
                <tr style="text-align:center"> 
                    <td width="25%">
                        <h3 class="tabel-title">Bayi</h3>
                    </td>
                    <td width="25%">
                        <h3 class="tabel-title">Pelajar</h3>
                    </td>
                    <td width="25%" class="best-seller-table best-seller-top" style="position:relative">
                        <img src="assets/images/best-seller.png" alt="best-seller" class="best-seller-logo">
                        <h3 class="tabel-title">Personal</h3>
                    </td>
                    <td width="25%">
                        <h3 class="tabel-title">Bisnis</h3>
                    </td>
                </tr>
                <tr style="text-align:center; vertical-align:middle"> 
                    <td width="25%">
                        <p class="price-before" id="price-bayi-before"></p>
                        <span>Rp <span class="price-after-front" id="price-bayi-after-front"></span><span class="price-after-end" id="price-bayi-after-end"></span><span>/bln</span></span>
                    </td>
                    <td width="25%">
                        <p class="price-before" id="price-pelajar-before"></p>
                        <span>Rp <span class="price-after-front" id="price-pelajar-after-front"></span><span class="price-after-end" id="price-pelajar-after-end"></span><span>/bln</span></span>
                    </td>
                    <td width="25%" class="best-seller-table best-seller-top">
                        <p class="price-before best-seller-top" id="price-personal-before"></p>
                        <span>Rp <span class="price-after-front" id="price-personal-after-front"></span><span class="price-after-end" id="price-personal-after-end"></span><span>/bln</span></span>
                    </td>
                    <td width="25%">
                        <p class="price-before" id="price-bisnis-before"></p>
                        <span>Rp <span class="price-after-front" id="price-bisnis-after-front"></span><span class="price-after-end" id="price-bisnis-after-end"></span><span>/bln</span></span>
                    </td>
                </tr>
                <tr style="text-align:center; vertical-align:middle"> 
                    <td width="25%">
                        <span class="tabel-pengguna" id="bayi-pengguna"></span><span> Pengguna Terdaftar</span>
                    </td>
                    <td width="25%">
                        <span class="tabel-pengguna" id="pelajar-pengguna"></span><span> Pengguna Terdaftar</span>
                    </td>
                    <td width="25%" class="best-seller-table best-seller-mid">
                        <span class="tabel-pengguna" id="personal-pengguna"></span><span> Pengguna Terdaftar</span>
                    </td>
                    <td width="25%">
                        <span class="tabel-pengguna" id="bisnis-pengguna"></span><span> Pengguna Terdaftar</span>
                    </td>
                </tr>
                <tr style="text-align:center; vertical-align:top" class="tabel-fitur"> 
                    <td width="25%">
                        <ul>
                            <li><b>0.5x RESOURCE POWER</b></li>
                            <li><b>500 MB</b> Disk Space</li>
                            <li><b>Unlimited</b> Bandwidth</li>
                            <li><b>Unlimited</b> Databases</li>
                            <li><b>1</b> Domain</li>
                            <li><b>Instant</b> Domain</li>
                            <li><b>Unlimited SSL</b> Gratis Selamanya</li>
                        </ul>
                    </td>
                    <td width="25%">
                        <ul>
                            <li><b>1x RESOURCE POWER</b></li>
                            <li><b>Unlimited</b> Disk Space</li>
                            <li><b>Unlimited</b> Bandwidth</li>
                            <li><b>Unlimited</b> POP3 Email</li>
                            <li><b>Unlimited</b> Databases</li>
                            <li><b>10</b> Addon Domain</li>
                            <li><b>Instant</b> Backup</li>
                            <li><b>Domain Gratis</b> Selamanya</li>
                            <li><b>Unlimited SSL</b> Gratis Selamanya</li>
                        </ul>
                    </td>
                    <td width="25%" class="best-seller-table">
                        <ul>
                            <li><b>2x RESOURCE POWER</b></li>
                            <li><b>Unlimited</b> Disk Space</li>
                            <li><b>Unlimited</b> Bandwidth</li>
                            <li><b>Unlimited</b> POP3 Email</li>
                            <li><b>Unlimited</b> Databases</li>
                            <li><b>Unlimited</b> Addon Domain</li>
                            <li><b>Instant</b> Backup</li>
                            <li><b>Domain Gratis</b> Selamanya</li>
                            <li><b>Unlimited SSL</b> Gratis Selamanya</li>
                            <li><b>Private</b> Name Server</li>
                            <li><b>SpamAssasin</b> Mail Protection</li>
                        </ul>
                    </td>
                    <td width="25%">
                        <ul>
                            <li><b>3x RESOURCE POWER</b></li>
                            <li><b>Unlimited</b> Disk Space</li>
                            <li><b>Unlimited</b> Bandwidth</li>
                            <li><b>Unlimited</b> POP3 Email</li>
                            <li><b>Unlimited</b> Databases</li>
                            <li><b>Unlimited</b> Addon Domain</li>
                            <li><b>Magic Auto</b> Backup & Restore</li>
                            <li><b>Domain Gratis</b> Selamanya</li>
                            <li><b>Unlimited SSL</b> Gratis Selamanya</li>
                            <li><b>Private</b> Name Server</li>
                            <li><b>Prioritas</b> Layanan Support</li>
                            <li><span class="fa fa-star text-primary"></span><span class="fa fa-star text-primary"></span><span class="fa fa-star text-primary"></span><span class="fa fa-star text-primary"></span><span class="fa fa-star text-primary"></span></li>
                            <li><b>SpamAssasin</b> Mail Protection</li>
                        </ul>
                    </td>
                </tr>
                <tr class="tabel-button"> 
                    <td width="25%">
                        <a href="" class="btn btn-default btn-pilih" >Pilih Sekarang</a>
                    </td>
                    <td width="25%">
                        <a href="" class="btn btn-default btn-pilih" >Pilih Sekarang</a>
                    </td>
                    <td width="25%" class="best-seller-table">
                        <a href="" class="btn btn-default btn-pilih btn-best" >Pilih Sekarang</a>
                    </td>
                    <td width="25%">
                        <a href="" class="btn btn-default btn-pilih" >Pilih Sekarang</a>
                    </td>
                </tr>
            </table>
            
        </div>
        </section>
        <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 py-2">
                    <h2 class="text-center limit-title">Powerful dengan Limit PHP yang Lebih Besar</h2>
                </div>
                <div class="col-md-6">
                    <table style="width:100%" class="text-center limit-tabel border table-striped">
                        <tr>
                            <td><span class="fa fa-check-circle text-success"></span></td>
                            <td class="limit-caption">
                                Max execution time 300s
                            </td>
                        </tr>
                        <tr>
                            <td><span class="fa fa-check-circle text-success"></span></td>
                            <td class="limit-caption">
                                Max execution time 300s
                            </td>
                        </tr>
                        <tr>
                            <td><span class="fa fa-check-circle text-success"></span></td>
                            <td class="limit-caption">
                                PHP memory limit 1024 MB
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                <table style="width:100%" class="text-center limit-tabel border table-striped">
                        <tr>
                            <td><span class="fa fa-check-circle text-success"></span></td>
                            <td class="limit-caption">
                                Post max size 128 MB
                            </td>
                        </tr>
                        <tr>
                            <td><span class="fa fa-check-circle text-success"></span></td>
                            <td class="limit-caption">
                                Upload max filesize 128 MB
                            </td>
                        </tr>
                        <tr>
                            <td><span class="fa fa-check-circle text-success"></span></td>
                            <td class="limit-caption">
                                Max input vars 2500
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
        </div>
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center hfeature-title">Semua Paket Hosting Sudah Termasuk</h2>
                    </div>
                </div>
                <table width="100%" border="0">
                    <tr>
                        <td width="33.3%">
                            <center>
                                <img class="hfeature-image" src="assets/svg/icon PHP Hosting_PHP Semua Versi.svg" alt="PHP ZenGuard Loader" >
                                <h3 class="hfeature-caption">PHP Semua Versi</h3>
                                <p class="hfeature-subcaption">Pilih mulai dari versi PHP 5.3 s/d PHP 7. Ubah sesuka anda!</p>
                            </center>    
                        </td>
                        <td width="33.3%">
                            <center>
                                <img class="hfeature-image" src="assets/svg/icon PHP Hosting_My SQL.svg" alt="MySQL" >
                                <h3 class="hfeature-caption">MySQL versi 5.6</h3>
                                <p class="hfeature-subcaption">Nikmati MySQL versi terbaru, tercepat dan kaya akan fitur.</p>
                            </center>    
                        </td>
                        <td width="33.3%">
                            <center>
                                <img class="hfeature-image" src="assets/svg/icon PHP Hosting_CPanel.svg" alt="CPanel" >
                                <h3 class="hfeature-caption">Panel Hosting CPanel</h3>
                                <p class="hfeature-subcaption">Kelola website dengan panel canggih yang familiar di hati anda.</p>
                            </center>    
                        </td>
                    </tr>
                    <tr>
                        <td width="33.3%">
                            <center>
                                <img class="hfeature-image" src="assets/svg/icon PHP Hosting_garansi uptime.svg" alt="Uptime" >
                                <h3 class="hfeature-caption">Garansi Uptime 99.9%</h3>
                                <p class="hfeature-subcaption">Data center yang mendukung kelangsungan website anda 24/7.</p>
                            </center>    
                        </td>
                        <td width="33.3%">
                            <center>
                                <img class="hfeature-image" src="assets/svg/icon PHP Hosting_InnoDB.svg" alt="InnoDB" >
                                <h3 class="hfeature-caption">Database InnoDB Unlimited</h3>
                                <p class="hfeature-subcaption">Jumlah dan ukuran database yang tumbuh sesuai kebutuhan anda.</p>
                            </center>    
                        </td>
                        <td width="33.3%">
                            <center>
                                <img class="hfeature-image" src="assets/svg/icon PHP Hosting_My SQL remote.svg" alt="MySQL Remote" >
                                <h3 class="hfeature-caption">Wildcard Remote MySQL</h3>
                                <p class="hfeature-subcaption">Mendukung s/d 25 max_user_connections dan 100 max_connections</p>
                            </center>    
                        </td>
                    </tr>
                </table>
            </div>
        </section>
        <section>
            <div class="container">
                <h2 class="text-center laravel-title">Mendukung Penuh Framework Laravel</h2>
                <table width="100%" border="0">
                    <tr>
                        <td width="50%">
                            <p class="laravel-caption">Tak perlu menggunakan dedicated server ataupun VPS yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda.</p>
                            <ul class="laravel-list">
                                <li><span class="fa fa-check-circle text-success"></span> Install laravel <b>1 klik</b> dengan Softaculous installer.</li>
                                <li><span class="fa fa-check-circle text-success"></span> Mendukung ekstensi <b>PHP MCrypt, phar, mbstring, json,</b> dan <b>fileinfo.</b></li>
                                <li><span class="fa fa-check-circle text-success"></span> Tersedia <b>Composer</b> dan <b>SSH</b> untuk menginstall packages pilihan Anda.</li>
                                <li class="laravel-nb">Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis</li>
                            </ul>
                            <a href="" class="btn btn-primary" style="border-radius:50px">Pilih Hosting Anda</a>
                        </td>
                        <td width="50%">
                            <img class="laravel-image banner-img float-right" src="assets/svg/illustration banner PHP hosting-01.svg">
                        </td>
                    </tr>
                </table>
            </div>
        </section>
        <section>
            <div class="container">
                <h2 class="text-center modul-title">Modul Lengkap Untuk Menjalankan Aplikasi Anda.</h2>
                <table width="100%" border="0" class="mx-auto modul-tabel">
                    <tr class="modul-text">
                        <td width="25%">
                            <ul>
                                <li>IcePHP</li>
                                <li>apc</li>
                                <li>apcu</li>
                                <li>apm</li>
                                <li>ares</li>
                                <li>bcmath</li>
                                <li>bcompiler</li>
                                <li>big_int</li>
                                <li>bitset</li>
                                <li>bloomy</li>
                                <li>bz2_filter</li>
                                <li>clamav</li>
                                <li>coin_acceptor</li>
                                <li>crack</li>
                                <li>dba</li>
                            </ul>
                        </td>
                        <td width="25%">
                            <ul>
                                <li>http</li>
                                <li>huffman</li>
                                <li>idn</li>
                                <li>igbinary</li>
                                <li>imagick</li>
                                <li>imap</li>
                                <li>inclued</li>
                                <li>inotify</li>
                                <li>interbase</li>
                                <li>intl</li>
                                <li>ioncube_loader</li>
                                <li>ioncube_loader_4</li>
                                <li>jsmin</li>
                                <li>json</li>
                                <li>ldap</li>
                            </ul>
                        </td>
                        <td width="25%">
                            <ul>
                                <li>nd_pdo_mysql</li>
                                <li>oauth</li>
                                <li>oci8</li>
                                <li>odbc</li>
                                <li>opache</li>
                                <li>pdf</li>
                                <li>pdo</li>
                                <li>pdo_dblib</li>
                                <li>pdo_firebird</li>
                                <li>pdo_mysql</li>
                                <li>pdo_odbc</li>
                                <li>pdo_pgsql</li>
                                <li>pdo_sqlite</li>
                                <li>pgsql</li>
                                <li>phalcon</li>
                            </ul>
                        </td>
                        <td width="25%">
                            <ul>
                                <li>stats</li>
                                <li>stem</li>
                                <li>stomp</li>
                                <li>suhosin</li>
                                <li>sybase_ct</li>
                                <li>sysvmsg</li>
                                <li>sysvsem</li>
                                <li>sysvshm</li>
                                <li>tidy</li>
                                <li>timezonedb</li>
                                <li>trader</li>
                                <li>translit</li>
                                <li>uploadprogress</li>
                                <li>uri_template</li>
                                <li>uuid</li>
                            </ul>
                        </td>
                    </tr>
                </table>
                <center>
                    <a href="" class="btn btn-default" style="border-radius:50px;border: 2px solid">Selengkapnya</a>
                </center>
            </div>
        </section>
        <section>
            <div class="container">
                <table width="100%" border="0">
                    <tr>
                        <td width="50%">
                            <h2 class="lve-title">Linux Hosting yang Stabil dengan Teknologi LVE</h2>
                            <p class="lve-caption">SuperMicro <b> Intel Xeon 24-Cores </b> server dengan RAM <b> 128 GB </b> dan teknologi <b> LVE CloudLinux </b> untuk stabilitas server Anda. Dilengkapi dengan <b> SSD </b> untuk kecepatan <b> MySQL </b> dan caching. Apache load balancer berbasis LiteSpeed Technologies, <b> CageFS </b> security, <b> Raid-10 </b> protection dan auto backup untuk keamanan website PHP Anda.</p>
                        </td>
                        <td width="50%">
                            <img class="lve-image banner-img float-right" src="assets/images/Image support.png">
                        </td>
                    </tr>
                </table>
                <a href="" style="border-radius:50px" class="btn btn-primary">Pilih Hosting Anda</a>
            </div>
        </section>
        <section class="share-section">
            <div class="container">
                <table width="100%" border="0">
                    <tr>
                        <td width="50%">
                            <p class="share-title">Bagikan jika Anda menyukai halaman ini.</p>
                        </td>
                        <td width="50%">
                            <a href="" class="fa fa-facebook-square text-primary"></a><small class="border py-1 px-2">80k</small>
                            <a href="" class="fa fa-twitter-square"></a><small class="border py-1 px-2">450</small>
                            <a href="" class="fa fa-google-plus-square text-danger"></a><small class="border py-1 px-2">1900</small>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
        <section class="section-help py-5">
            <div class="container">
                <table width="100%" border="0">
                    <tr>
                        <td width="80%">
                            <h2 class="help-title">Perlu <b>BANTUAN?</b> Hubungi Kami : <b>0274-5305505</b></h2>
                        </td>
                        <td width="20%">
                            <a href="" class="btn btn-default" style="color:white;border-radius:50px;border: 2px solid white"><i class="fa fa-comments"></i> Live Chat</a>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
        <section class="section-footer">
            <div class="container">
                <table width="100%">
                    <tr style="vertical-align:top">
                        <td width="25%">
                            <ul>
                                <li class="footer-title"><b>HUBUNGI KAMI</b></li>
                                <li>0274-5305505</li>
                                <li>Senin-Minggu</li>
                                <li>24 Jam NonStop</li>
                                <br>
                                <li>Jl. Selokan Mataram Monjali</li>
                                <li>Karangjati MT I/304</li>
                                <li>Sinduadi, Mlati, Sleman</li>
                                <li>Yogyakarta 55284</li>
                            </ul>
                        </td>
                        <td width="25%">
                            <ul>
                                <li class="footer-title"><b>LAYANAN</b></li>
                                <li><a href="">Domain</a></li>
                                <li><a href="">Shared Hosting</a></li>
                                <li><a href="">Cloud VPS Hosting</a></li>
                                <li><a href="">Managed VPS Hosting</a></li>
                                <li><a href="">Web Builder</a></li>
                                <li><a href="">Keamanan SSL / HTTPS</a></li>
                                <li><a href="">Jasa Pembuatan Website</a></li>
                                <li><a href="">Program Afiliasi</a></li>
                            </ul>
                        </td>
                        <td width="25%">
                            <ul>
                                <li class="footer-title"><b>SERVICE HOSTING</b></li>
                                <li><a href="">Hosting Murah</a></li>
                                <li><a href="">Hosting Indonesia</a></li>
                                <li><a href="">Hosting Singapura SG</a></li>
                                <li><a href="">Hosting PHP</a></li>
                                <li><a href="">Hosting Wordpress</a></li>
                                <li><a href="">Hosting Laravel</a></li>
                            </ul>
                        </td>
                        <td width="25%">
                            <ul>
                                <li class="footer-title"><b>TUTORIAL</b></li>
                                <li><a href="">Knowledgebase</a></li>
                                <li><a href="">Blog</a></li>
                                <li><a href="">Cara Pembayaran</a></li>
                            </ul>
                        </td>
                    </tr>
                    <tr style="vertical-align:top">
                        <td width="25%">
                            <ul>
                                <li class="footer-title"><b>TENTANG KAMI</b></li>
                                <li><a href="">Team Niagahoster</a></li>
                                <li><a href="">Karir</a></li>
                                <li><a href="">Events</a></li>
                                <li><a href="">Penawaran & Promo Spesial</a></li>
                                <li><a href="">Kontak Kami</a></li>
                            </ul>
                        </td>
                        <td width="25%">
                            <ul>
                                <li class="footer-title"><b>KENAPA PILIH NIAGAHOSTER</b></li>
                                <li>Support Terbaik</li>
                                <li>Garansi Harga Termurah</li>
                                <li>Domain Gratis Selamanya</li>
                                <li>Datacenter Hosting Terbaik</li>
                                <li>Review Pelanggan</li>
                            </ul>
                        </td>
                        <td width="25%">
                            <ul>
                                <li class="footer-title"><b>NEWSLETTER</b></li>
                                <li>
                                    <form action="" class="px-3">
                                        <div class="input-group input">
                                            <input type="text" name="email" placeholder="Email" class="form-control" style="border-radius:50px">
                                            <button class="btn btn-primary btn-news" style="border-radius:50px"> Berlangganan</button>
                                        </div>
                                    </form>
                                    <p>Dapatkan promo dan konten menarik dari penyedia hosting terbaik Anda.</p>
                                </li>
                            </ul>
                        </td>
                        <td width="25%">
                            <a href="" class="fa fa-facebook footer-icon"></a>
                            <a href="" class="fa fa-twitter footer-icon"></a>
                            <a href="" class="fa fa-google footer-icon"></a>
                        </td>
                    </tr>
                    <tr style="vertical-align:top">
                        <td width="25%">
                            <ul>
                                <li class="footer-title">
                                    <b>PEMBAYARAN</b>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="mr-3 rounded">
                                <img src="assets/svg/payment/bca.svg" class="payment-icon" alt="">
                                <img src="assets/svg/payment/mandiri.svg" class="payment-icon" alt="">
                                <img src="assets/svg/payment/bni.svg" class="payment-icon" alt="">
                                <img src="assets/svg/payment/visa.svg" class="payment-icon" alt="">
                                <img src="assets/svg/payment/mastercard.svg" class="payment-icon" alt="">
                                <img src="assets/svg/payment/bersama.svg" class="payment-icon" alt="">
                                <img src="assets/svg/payment/permata.svg" class="payment-icon" alt="">
                                <img src="assets/svg/payment/prima.svg" class="payment-icon" alt="">
                                <img src="assets/svg/payment/alto.svg" class="payment-icon" alt="">
                            </div>
                            <p class="footer-title">Aktivasi instan dengan e-Payment. Hosting dan domain langsung aktif!</p>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
        <section class="section-copyright">
            <div class="container">
                <table width="100%">
                    <tr>
                        <td width="70%">
                            <p>
                                Copyright ©2016 Niagahoster | Hosting powered by PHP7,CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta<br>
                                Cloud VPS Murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology
                            </p>
                        </td>
                        <td width="30%">
                            <span><a href="">Syarat dan Ketenuan</a>|<a href="">Kebijakan Privasi</a></span>
                        </td>
                    </tr>     
                </table>            
            </div>
        </section>

        <script>
            $(document).ready(function () {
                $.get("assets/json/price.json", function(data) {
                    // PRICE SECTION
                    $("#price-bayi-before").html(data.bayi.before)
                    var bayiafter = data.bayi.after.split(".")
                    $("#price-bayi-after-front").html(bayiafter[0])
                    $("#price-bayi-after-end").html("." + bayiafter[1])

                    $("#price-pelajar-before").html(data.pelajar.before)
                    var pelajarafter = data.pelajar.after.split(".")
                    $("#price-pelajar-after-front").html(pelajarafter[0])
                    $("#price-pelajar-after-end").html("." + pelajarafter[1])

                    $("#price-personal-before").html(data.personal.before)
                    var personalafter = data.personal.after.split(".")
                    $("#price-personal-after-front").html(personalafter[0])
                    $("#price-personal-after-end").html("." + personalafter[1])

                    $("#price-bisnis-before").html(data.bisnis.before)
                    var bisnisafter = data.bisnis.after.split(".")
                    $("#price-bisnis-after-front").html(bisnisafter[0])
                    $("#price-bisnis-after-end").html("." + bisnisafter[1])

                    // REGISTERED SECTION
                    $("#bayi-pengguna").html(data.bayi.pengguna)
                    $("#pelajar-pengguna").html(data.pelajar.pengguna)
                    $("#personal-pengguna").html(data.personal.pengguna)
                    $("#bisnis-pengguna").html(data.bisnis.pengguna)
                })
            })
        </script>
    </body>
</html>